import os
import shutil
from glob import glob
import cv2
import numpy as np
import torch

from util.metrics import PSNR, SSIM
from sewar.full_ref import psnr, ssim

def fname(k, n):
  return f'k{k}_n{n}'

def makeDir(dir):
    if os.path.exists(dir):
        shutil.rmtree(dir)    
    os.makedirs(dir, exist_ok=True)

def searchImgs(dir, files=[]):
    if len(files) > 0:
        return sorted(os.path.join(dir, file) for file in files)
    else:
        return sorted(glob(os.path.join(dir, '*.tif')))

def normalizeGrayU8(img):
    cv2.normalize(img, img, 0, 255, cv2.NORM_MINMAX, dtype=-1)
    return np.uint8(img)

def grayToTensor(img):
    ''' Shape of image from (x, y, n_ch) 
        => (n_ch, x, y) 
        => (1, n_ch, x, y) 
    '''
    imgt = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    imgt = imgt / 255.
    imgt = np.expand_dims(np.moveaxis(imgt, -1, 0), 0)
    imgt = torch.from_numpy(imgt).float()
    return imgt

def computeMetrics(img1, img2, metric='db'):
    psnrv = 0
    ssimv = 0
    if metric=='db':
        psnrv = PSNR(img1, img2)

        img1t = grayToTensor(img1)
        img2t = grayToTensor(img2)

        ssimv = SSIM(img1t, img2t).item()
    elif metric=='sewar':
        max = img1.max()

        psnrv = psnr(img1, img2, MAX=max)
        ssimv, _ = ssim(img1, img2, ws=11, MAX=max)

    return [psnrv, ssimv]

def formatString(psnrv, ssimv):
    return f"PSNR: {psnrv: .3f} - SSIM: {ssimv: .3f}"